# -*- coding: utf-8 -*-

import io
import os

from argparse import ArgumentParser

import numpy as np
import pandas as pd

from num2words import num2words

from information_collector.email import send
from information_collector.excel import set_autosize, set_format_to_column
from information_collector.web_crawler import download_webdriver, get_currency_pair

# функция для формирования строки в отчетном документе
def get_final_row(prefix, number):
    number_in_str = num2words(number, lang='ru')
    # number_str = str(number)
    last_digit_str = str(number)[-1]
    last_digit_int = int(last_digit_str)

    # 0
    if not number:
        return '{} нет строк'.format(prefix)

    if 4 < number < 21:
        return '{} {} строк'.format(prefix, number_in_str)

    if 0 < last_digit_int < 5:
        if last_digit_int == 1:
            return '{} {}на строка'.format(prefix, number_in_str[:-2])
        elif last_digit_int == 2:
            return '{} {}е строки'.format(prefix, number_in_str[:-1])
        else:
            # 3, 4
            return '{} {} строки'.format(prefix, number_in_str)
    # 5,6,7,8,9,0
    else:
        return '{} {} строк'.format(prefix, number_in_str)


def get_parser():
    parser = ArgumentParser()
    parser.add_argument('--email-to', required=False, help = 'Email to send to')
    parser.add_argument('--subject', default='Report', help='An email subject')
    parser.add_argument('--attachment-filename', default='report.xlsx', help='An attached file name')     
    return parser

def main():
    args_parser = get_parser()
    args = args_parser.parse_args()
    print('Collect data from site')
    with download_webdriver('win' if os.name == 'nt' else 'linux') as driver:
         currency = get_currency_pair('USD_RUB', 'EUR_RUB', driver_path=driver)
    df_usd = pd.DataFrame(currency['USD_RUB'], columns=['Дата USD', 'Курс USD/RUB']).sort_values('Дата USD', ascending=False).reindex()
    df_usd['Курс USD/RUB'] = df_usd['Курс USD/RUB'].str.replace('-', '0')
    df_usd['Курс USD/RUB'] = df_usd['Курс USD/RUB'].str.replace(',', '.').astype(float)
    df_usd['Изменение USD'] = (df_usd['Курс USD/RUB'] - df_usd['Курс USD/RUB'].shift(-1)).fillna(0)

    df_eur = pd.DataFrame(currency['EUR_RUB'], columns=['Дата EUR', 'Курс EUR/RUB']).sort_values('Дата EUR', ascending=False).reindex()
    df_eur['Курс EUR/RUB'] = df_eur['Курс EUR/RUB'].str.replace('-', '0')
    df_eur['Курс EUR/RUB'] = df_eur['Курс EUR/RUB'].str.replace(',', '.').astype(float)
    df_eur['Изменение EUR'] = (df_eur['Курс EUR/RUB'] - df_eur['Курс EUR/RUB'].shift(-1)).fillna(0)

    # sql outer join
    res = df_usd.merge(df_eur, left_index=True, right_index=True, how='outer')
    res['Отношение'] = res['Курс EUR/RUB'] / res['Курс USD/RUB']
    res.loc[~np.isfinite(res['Отношение']), 'Отношение'] = 0
    res['Отношение'] = res['Отношение'].round(4)
    # res['Отношение'] = '=IFERROR(INDIRECT("R[0]C[{}]", 0) / INDIRECT("R[0]C[{}]", 0), 0)'.format(
    #         res.columns.get_loc('Курс EUR/RUB') - res.columns.get_loc('Отношение'),
    #         res.columns.get_loc('Курс USD/RUB') - res.columns.get_loc('Отношение'),
    #         )
    output = io.BytesIO()
    writer = pd.ExcelWriter(output, engine='openpyxl')
    res.to_excel(writer, index=False)
    set_format_to_column(writer.book.active, 'Курс USD/RUB', 'Изменение USD', 'Курс EUR/RUB', 'Изменение EUR')
    for col in writer.book.active.columns:
        if col[0].value.startswith('Дата'):
            col[0].value = 'Дата'
        if col[0].value.startswith('Изменение'):
            col[0].value = 'Изменение'
    set_autosize(writer.book.active)
    writer.save()
    
    # send email
    email_from = os.environ.get('MY_MAIL', 'mailForRobotR2D2@gmail.com')
    password = os.environ.get('MY_MAIL_PASSWORD', 'der_parol')
    text = get_final_row('В прикрепленном файле excel ', len(res) + 1)
    send(args.subject, text, (args.attachment_filename, output), email_from, password, args.email_to or email_from)
    print(res)
    print('Job is done')
