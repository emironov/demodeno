# -*- coding: utf-8 -*-

RUB_CURRENCY = '_-* #,##0.00\ "₽"_-;\-* #,##0.00\ "₽"_-;_-* "-"??\ "₽"_-;_-@_-'

from  openpyxl.utils import get_column_letter
def get_col_by_name(sheet, col_name):
    return next(filter(lambda col: col[0].value == col_name, sheet.columns))

def set_format_to_column(sheet, *col_names, fmt=RUB_CURRENCY):
    for c_name in col_names:
        for cell in get_col_by_name(sheet, c_name):
            cell.number_format = fmt

def set_autosize(sheet):
    column_letters = tuple(get_column_letter(col_number + 1) for col_number in range(sheet.max_column))
    for column_letter in column_letters:
        sheet.column_dimensions[column_letter].bestFit = True
        sheet.column_dimensions[column_letter].auto_size = True
